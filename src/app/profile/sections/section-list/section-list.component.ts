import { Component, OnInit } from '@angular/core';
import {ISection} from '../section.model';

@Component({
  selector: 'app-section-list',
  templateUrl: './section-list.component.html',
  styleUrls: ['./section-list.component.scss']
})
export class SectionListComponent implements OnInit {
  sectionList = Array<ISection>();

  constructor() {
    this.sectionList.push({id: 0, title: 'Резюме', content: 'text text text'});
    this.sectionList.push({id: 1, title: 'Проекты', content: 'text text text'});
  }

  ngOnInit() {

  }

}
