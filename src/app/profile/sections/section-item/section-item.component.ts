import { Component, OnInit } from '@angular/core';
import {ISection} from '../section.model';

@Component({
  selector: 'app-section-item',
  templateUrl: './section-item.component.html',
  styleUrls: ['./section-item.component.scss']
})
export class SectionItemComponent implements OnInit {
  section: ISection;

  constructor() { }

  ngOnInit() {
  }

}
